public class Fibonacci {
public void getFibonacci(int pos) {
                for (int i = 1; i <= pos; i++) {
                        String fibonacci = recursive(i) + "";
                        if (i == pos)
                                fibonacci += " ";
                        else
                                fibonacci += " ";
                        System.out.print(fibonacci);
                }
        }

private int recursive(int pos) {
		if (pos == 1) {
			return 1;
		} else if (pos == 2) {
			return 1;
		} else {
			return recursive(pos - 1) + recursive(pos - 2);
		}
	}

public static void main(String[] args) {
		new Fibonacci().getFibonacci(5);
	}
}
